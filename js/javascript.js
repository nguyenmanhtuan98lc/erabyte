function openNav() {
    document.getElementById("mySidebar").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidebar").style.width = "0";
}

function routeTC() {
    window.location.href = "#ALVIS-1";
}

function routeEC() {
    window.location.href = "#ALVIS-2";
}

function routeSC() {
    window.location.href = "#ALVIS-3";
}

function routePTZ() {
    window.location.href = "#ALVIS-4";
}

function routeEP() {
    window.location.href = "#ALVIS-5";
}

function TCInViewport() {
    var myElement = document.getElementById('ALVIS-TC');
    var bounding = myElement.getBoundingClientRect();
    if (bounding.top >= 0 && bounding.left >= 0 && bounding.right <= (window.innerWidth || document.documentElement.clientWidth) && bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)) {
        return true;
    } else {
        return false;
    }
}

function ECInViewport() {
    var myElement = document.getElementById('ALVIS-EC');
    var bounding = myElement.getBoundingClientRect();
    if (bounding.top >= 0 && bounding.left >= 0 && bounding.right <= (window.innerWidth || document.documentElement.clientWidth) && bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)) {
        return true;
    } else {
        return false;
    }
}

function SCInViewport() {
    var myElement = document.getElementById('ALVIS-SC');
    var bounding = myElement.getBoundingClientRect();
    if (bounding.top >= 0 && bounding.left >= 0 && bounding.right <= (window.innerWidth || document.documentElement.clientWidth) && bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)) {
        return true;
    } else {
        return false;
    }
}

function PTZCInViewport() {
    var myElement = document.getElementById('ALVIS-PTZC');
    var bounding = myElement.getBoundingClientRect();
    if (bounding.top >= 0 && bounding.left >= 0 && bounding.right <= (window.innerWidth || document.documentElement.clientWidth) && bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)) {
        return true;
    } else {
        return false;
    }
}

function EPInViewport() {
    var myElement = document.getElementById('ALVIS-EP');
    var bounding = myElement.getBoundingClientRect();
    if (bounding.top >= 0 && bounding.left >= 0 && bounding.right <= (window.innerWidth || document.documentElement.clientWidth) && bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight)) {
        return true;
    } else {
        return false;
    }
}

window.addEventListener("scroll", checkRadio);

function checkRadio() {
    if (TCInViewport() == true) {
        document.getElementById('route-1').checked = true;
    }
    else if (ECInViewport() == true) {
        document.getElementById('route-2').checked = true;
    }
    else if (SCInViewport() == true) {
        document.getElementById('route-3').checked = true;
    }
    else if (PTZCInViewport() == true) {
        document.getElementById('route-4').checked = true;
    }
    else if (EPInViewport() == true) {
        document.getElementById('route-5').checked = true;
    }
    else {
        void(0);
    }    
}
